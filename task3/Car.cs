﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class Car : Vehicle
    {
        public Car(string coordinate, double price, double speed, int yearOfManufacture) :
            base(coordinate, price, speed, yearOfManufacture)
        {
            
        }
        public override void Show()
        {
            Console.WriteLine("Car:");
            base.Show();
        }
    }
}
