﻿namespace task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Plane plane = new Plane(123, 100, "air", 140, 300, 2014);
            plane.Show();

            Ship ship = new Ship(123, "Alabasta", "water", 150, 400, 2015);
            ship.Show();

            Car car = new Car("ground", 160, 500, 2016);
            car.Show();
        }
    }
}