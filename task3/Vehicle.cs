﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class Vehicle
    {
        private string coordinate;
        private double price;
        private double speed;
        private int yearOfManufacture;

        public Vehicle() 
        {
            coordinate = string.Empty;
        }

        public Vehicle(string coordinate, double price, double speed, int yearOfManufacture)
        {
            this.coordinate = coordinate;
            this.price = price;
            this.speed = speed;
            this.yearOfManufacture = yearOfManufacture;
        }

        public virtual void Show()
        {
            Console.WriteLine("\tCoordinates: " + coordinate);
            Console.WriteLine("\tprice: " + price);
            Console.WriteLine("\tspeed: " + speed);
            Console.WriteLine("\tpassengers: " + yearOfManufacture);
        }
    }
}
