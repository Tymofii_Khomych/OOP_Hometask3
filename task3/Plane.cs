﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class Plane : Vehicle
    {
        private double flightAltitude;
        private int pasangers;
   
        public Plane(double flightAltitude, int pasangers, string coordinate, double price, double speed, int yearOfManufacture) : 
            base(coordinate, price, speed, yearOfManufacture)
        {
            this.flightAltitude = flightAltitude;
            this.pasangers = pasangers;
        }

        public override void Show()
        {
            Console.WriteLine("Plane:");
            Console.WriteLine("\tFlight altitude: " + flightAltitude);
            Console.WriteLine("\tNumber of pasangers: " + pasangers);
            base.Show();
        }
    }
}
