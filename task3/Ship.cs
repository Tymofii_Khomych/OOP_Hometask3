﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class Ship : Vehicle
    {
        private int pasangers;
        private string portname;

        public Ship()
        {
            portname = string.Empty;
        }

        public Ship(int pasangers, string portname, string coordinate, double price, double speed, int yearOfManufacture)
            : base(coordinate, price, speed, yearOfManufacture)
        {
            this.pasangers = pasangers;
            this.portname = portname;
        }

        public override void Show() 
        {
            Console.WriteLine("Ship:");
            Console.WriteLine("\tpasangers: " + pasangers);
            Console.WriteLine("\tportname: " + portname);
            base.Show();
        }
    }
}
