﻿namespace task5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Printer printer = new Printer();
            printer.Print("Simple text printed by default printer");

            Printer colorPrinter = new ColorPrinter();
            colorPrinter.Print("Colorful text printed by colorful printer");

        }
    }
}