﻿namespace task4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            Console.Write("Input key: ");
            string key = Console.ReadLine();
            Console.WriteLine();

            switch (key)
            {
                case "pro":
                    DocumentWorker proDocumentWorker = new ProDocumentWorker();
                    proDocumentWorker.OpenDocument();
                    proDocumentWorker.EditDocument();
                    proDocumentWorker.SaveDocument();
                    break;

                case "exp":
                    DocumentWorker expertDocumentWorker = new ExpertDocumentWorker();

                    expertDocumentWorker.OpenDocument();
                    expertDocumentWorker.EditDocument();
                    expertDocumentWorker.SaveDocument();
                    break;

                default:
                    DocumentWorker worker = new DocumentWorker();

                    worker.OpenDocument();
                    worker.EditDocument();
                    worker.SaveDocument();
                    break;
            }
        }
    }
}