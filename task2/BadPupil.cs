﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class BadPupil : Pupil
    {
        public override void Study()
        {
            Console.WriteLine("\tBad pupil never studing");
        }
        public override void Read()
        {
            Console.WriteLine("\tBad pupil never reading");
        }
        public override void Write()
        {
            Console.WriteLine("\tBad pupil never writing");
        }
        public override void Relax()
        {
            Console.WriteLine("\tBad pupil always relaxing");
        }
    }
}
