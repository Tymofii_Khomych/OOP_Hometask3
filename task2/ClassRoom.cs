﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class ClassRoom
    {
        private Pupil[] pupils;

        public ClassRoom(Pupil pupil1, Pupil pupil2, Pupil pupil3 = null, Pupil pupil4 = null)
        {
            pupils = new Pupil[] { pupil1, pupil2 };
            int size = 2;

            if (pupil3 != null)
            {
                Array.Resize(ref pupils, size += 1);
                pupils[size - 1] = pupil3;
            }

            if (pupil4 != null)
            {
                Array.Resize(ref pupils, size += 1);
                pupils[size - 1] = pupil4;
            }
        }


        public void ShowClass()
        {
            for (int i = 0; i < pupils.Length; i++)
            {
                Console.WriteLine($"Pupil {i + 1}\n\t");
                pupils[i].Study();
                pupils[i].Read();
                pupils[i].Write();
                pupils[i].Relax();
            }
        }
    }
}
