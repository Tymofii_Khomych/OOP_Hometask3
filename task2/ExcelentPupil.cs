﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class ExcelentPupil : Pupil
    {
        public override void Study()
        {
            Console.WriteLine("\tExcelent pupil always studing");
        }
        public override void Read()
        {
            Console.WriteLine("\tExcelent pupil always reading");
        }
        public override void Write()
        {
            Console.WriteLine("\tExcelent pupil always writing");
        }
        public override void Relax() 
        {
            Console.WriteLine("\tExcelent pupil never relaxing");
        }
    }
}
