﻿namespace task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Pupil pupil1 = new BadPupil();
            Pupil pupil2 = new GoodPupil();
            Pupil pupil3 = new GoodPupil();
            Pupil pupil4 = new ExcelentPupil();

            ClassRoom classroom1 = new ClassRoom(pupil1, pupil2, pupil3, pupil4);
            classroom1.ShowClass();

            //ClassRoom classroom2 = new ClassRoom(pupil1, pupil2, pupil3);
            //classroom2.ShowClass();

            //ClassRoom classroom3 = new ClassRoom(pupil1, pupil2);
            //classroom3.ShowClass();
        }
    }
}