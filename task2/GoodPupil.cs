﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class GoodPupil : Pupil
    {
        public override void Study()
        {
            Console.WriteLine("\tGood pupil sometimes studing");
        }
        public override void Read()
        {
            Console.WriteLine("\tGood pupil sometimes reading");
        }
        public override void Write()
        {
            Console.WriteLine("\tGood pupil sometimes writing");
        }
        public override void Relax()
        {
            Console.WriteLine("\tGood pupil sometimes relaxing");
        }
    }
}
