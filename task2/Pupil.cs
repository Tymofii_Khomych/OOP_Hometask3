﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class Pupil
    {
        public virtual void Study()
        {
            Console.WriteLine("\tStudying");
        }
        public virtual void Read()
        {
            Console.WriteLine("\tReading");
        }
        public virtual void Write()
        {
            Console.WriteLine("\tWriting");
        }
        public virtual void Relax()
        {
            Console.WriteLine("\tRelaxing");
        }
    }
}
